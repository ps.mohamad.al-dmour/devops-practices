#!/usr/bin/env bash

branchName=$CI_COMMIT_REF_NAME
releaseName=assignment-$branchName
hostName=$releaseName.local
commitId=$(git rev-parse --verify HEAD | cut -c1-8)
commidDate=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%y%m%d)
tag=$commidDate-$commitId
namespace=assignment

export KUBECONFIG=./cicd/k8sconfig
kubectl cluster-info
kubectl create secret docker-registry docker-reg -n $namespace --docker-server=https://registry.hub.docker.com/v1/ --docker-username="$DOCKER_USER" --docker-password="$DOCKER_PASSWORD" --docker-email=hamodehdmour@gmail.com
echo $releaseName
pwd
helm upgrade --install $releaseName assignment --wait --namespace $namespace --set app.image.tag=$tag --set app.ingress.hosts=$hostName