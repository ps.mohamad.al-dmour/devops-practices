#!/usr/bin/env bash

branchName=$CI_COMMIT_REF_NAME
releaseName=assignment-$branchName

export KUBECONFIG=cicd/k8sconfig
helm delete --purge $releaseName
