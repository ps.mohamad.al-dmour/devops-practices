#!/usr/bin/env bash

commitId=$(git rev-parse --verify HEAD | cut -c1-8)
commidDate=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%y%m%d)
tag=$commidDate-$commitId

docker login -u $DOCKER_USER -p $DOCKER_PASSWORD
docker build -t mohdmour/practice_devops:$tag .
docker tag mohdmour/practice_devops mohdmour/practice_devops:$tag
docker push mohdmour/practice_devops:$tag
