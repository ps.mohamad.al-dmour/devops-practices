FROM java:8-jdk-alpine

WORKDIR /app

COPY --chown=nobody:nobody devops_sample/target/*.jar ./assignment.jar

EXPOSE 8090

USER 65534

CMD java -jar -Dserver.port=8090 -Dspring.profiles.active=sql assignment.jar